import ballerina/io;
import ballerina/http;
import ballerina/sql;
import ballerinax/mysql;

//Database connection  
string dbUser = "root";
string dbPassword = "Test@123";

function initializeClients() returns sql:Error? {
    mysql:Client|sql:Error mysqlClient1 = new ();
    if (mysqlClient1 is sql:Error) {
        io:println("Error when initializing the MySQL client without any " +
            "params. ", mysqlClient1.message());
    } else {
        io:println("Simple MySQL client created successfully");
        check mysqlClient1.close();
    }

    mysql:Client mysqlClient2 = check new ("localhost", dbUser, dbPassword);
    io:println("MySQL client with user and password created.");

    mysql:Client mysqlClient3 = check new (user = dbUser,
        password = dbPassword);
    io:println("MySQL client with user and password created " +
        "with default host.");

    mysql:Client mysqlClient4 = check new ("localhost", dbUser, dbPassword,
        "information_schema", 3306);
    io:println("MySQL client with host, user, password, database and " +
        "port created.");

public function main() {

    
    io:println("Worker execution started");
//workers calculating the results of votes 
    worker w1 {
        http:Client httpClient = checkpanic new ("https://api.mathjs.org");
        var response = httpClient->get("/ SELECT MAX(Votes)  
FROM Votes.Results;  
GO ");
        if response is http:Response {
            io:println("Worker 1 response: ", response.getTextPayload());
        }
    }

    worker w2 {
        http:Client httpClient = checkpanic new ("https://api.Results.org");
        var response = httpClient->get("/ SELECT  MIN(votes )");
        if response is http:Response {
            io:println("Worker 2 response: ", response.getTextPayload());
        }
    }
    _ = wait {w1, w2};

    io:println("Worker execution finished");
}
